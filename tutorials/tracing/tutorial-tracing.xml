<book>

<chapter id="TRACING">
<title>Tracing bitmaps</title>


<abstract>
<para>
One of the features in Inkscape is a tool for tracing a bitmap image
into a &lt;path> element for your SVG drawing.  These short notes
should help you become acquainted with how it works.
</para>
</abstract>

<sect1>
<para>
Currently Inkscape employs the Potrace bitmap tracing engine (<ulink
url="http://potrace.sourceforge.net">potrace.sourceforge.net</ulink>) by Peter Selinger.
In the future we expect to allow alternate tracing programs; for now, however, this fine
tool is more than sufficient for our needs.
</para>

<para>
Keep in mind that the Tracer's purpose is not to reproduce an exact duplicate of the
original image; nor is it intended to produce a final product. No autotracer can do
that. What it does is give you a set of curves which you can use as a resource for your
drawing.
</para>

<para>
Potrace interprets a black and white bitmap, and produces a set of curves. For Potrace,
we currently have three types of input filters to convert from the raw image to
something that Potrace can use.
</para>

<para>
Generally the more dark pixels in the intermediate bitmap, the more tracing that Potrace
will perform.  As the amount of tracing increases, more CPU time will be required, and
the &lt;path> element will become much larger.  It is suggested that the user experiment
with lighter intermediate images first, getting gradually darker to get the desired
proportion and complexity of the output path.
</para>

<para>
To use the tracer, load or import an image, select it,
and select the <command>Path &gt; Trace Bitmap</command> item, or <keycap>Shift+Alt+B</keycap>.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="tracing-f01.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
The user will see the three filter options available:
</para>

<itemizedlist>
<listitem><para>
Brightness Cutoff
</para></listitem>
</itemizedlist>

<para>
This merely uses the sum of the red, green and blue (or shades of gray) of a pixel as an
indicator of whether it should be considered black or white.  The threshold can be set
from 0.0 (black) to 1.0 (white).  The higher the threshold setting, the fewer the number
pixels that will be considered to be “white”, and the intermediate image with
become darker.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="tracing-f02.svg"/>
</imageobject>
</mediaobject>
</figure>

<itemizedlist>
<listitem><para>
Edge Detection
</para></listitem>
</itemizedlist>

<para>
This uses the edge detection algorithm devised by J. Canny as a way of quickly finding
isoclines of similar contrast.  This will produce an intermediate bitmap that will look
less like the original image than does the result of Brightness Threshold, but will
likely provide curve information that would otherwise be ignored.  The threshold setting
here (0.0 – 1.0) adjusts the brightness threshold of whether a pixel adjacent to a
contrast edge will be included in the output.  This setting can adjust the darkness or
thickness of the edge in the output.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="tracing-f03.svg"/>
</imageobject>
</mediaobject>
</figure>

<itemizedlist>
<listitem><para>
Color Quantization
</para></listitem>
</itemizedlist>

<para>
The result of this filter will produce an intermediate image that is very different from
the other two, but is very useful indeed.  Instead of showing isoclines of brightness or
contrast, this will find edges where colors change, even at equal brightness and
contrast.  The setting here, Number of Colors, decides how many output colors there
would be if the intermediate bitmap were in color.  It then decides black/white on
whether the color has an even or odd index.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="tracing-f04.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
The user should try all three filters, and observe the different types of output for
different types of input images.  There will always be an image where one works better
than the others.
</para>

<para>
After tracing, it is also suggested that the user try <command>Path &gt; Simplify</command>
(<keycap>Ctrl+L</keycap>) on the output path to reduce the number of nodes.  This can
make the output of Potrace much easier to edit.  For example, here is a typical tracing
of the Old Man Playing Guitar:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="tracing-f05.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Note the enormous number of nodes in the path.  After hitting <keycap>Ctrl+L</keycap>,
this is a typical result:
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="tracing-f06.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
The representation is a bit more approximate and rough, but the drawing is much simpler
and easier to edit.  Keep in mind that what you want is not an exact rendering of the
image, but a set of curves that you can use in your drawing.
</para>
</sect1>
</chapter>
</book>
